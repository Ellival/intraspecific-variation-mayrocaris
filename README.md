# Intraspecific variation of *Mayrocaris bucculata*

Data from Laville *et al.* 2021. Morphology and anatomy of the Late Jurassic *Mayrocaris bucculata</i>* (Eucrustacea?, Thylacocephala) with comments on the tagmosis of Thylacocephala. Journal of Systematic Palaeontology, 19(4):289-320. https://doi.org/10.1080/14772019.2021.1910584

## Data

All datas are available on the Zenodo repository: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4600805.svg)](https://doi.org/10.5281/zenodo.4600805)

## Data acquisition
Shield and appendages measurements for 28 specimens of *Mayrocaris bucculata* from Solnhofen Lagerstätte (Lower Tithonian, Upper Jurassic). Measurements were directly done on digital photographs with ImageJ2. Script for descriptive statistics and for the study of size allometry is provided.

**Measurement abbreviations**. **Aad**, anterodorsal angle; **Aav**, anteroventral angle; **Apd**, posterodorsal angle; **Apv**, posteroventral angle; **ha**, anterior shield height; **hmax**, maximum shield height; **hp**, posterior shield height; **ls**, length of the shield; **l**, length of element (appendages); **ra1-3**, raptorial appendages 1–3; **rae1-5**, elements 1–5 of raptorial appendages; **w**, width of element (appendages).
